import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class surgery implements Serializable {
    private String name;
    private String postcode;
    private LocalTime openingTime;
    private LocalTime closingTime;
    private LocalTime trainingStart;
    private LocalTime trainingEnd;

    public surgery(String name, String postcode, LocalTime openingTime, LocalTime closingTime, LocalTime trainingStart, LocalTime trainingEnd) {
        this.name = name;
        this.postcode = postcode;
        this.openingTime = openingTime;
        this.closingTime = closingTime;
        this.trainingStart = trainingStart;
        this.trainingEnd = trainingEnd;
    }

    public LocalTime getTrainingEnd() {
        return trainingEnd;
    }

    public String getName() {
        return name;
    }

    public String getPostcode() {
        return postcode;
    }

    public LocalTime getOpeningTime() {
        return openingTime;
    }

    public LocalTime getClosingTime() {
        return closingTime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public void setOpeningTime(LocalTime openingTime) {
        this.openingTime = openingTime;
    }

    public void setClosingTime(LocalTime closingTime) {
        this.closingTime = closingTime;
    }

    public LocalTime getTrainingStart() {
        return trainingStart;
    }

    public void setTrainingStart(LocalTime trainingStart) {
        this.trainingStart = trainingStart;
    }

    public void setTrainingEnd(LocalTime trainingEnd) {
        this.trainingEnd = trainingEnd;
    }
}
