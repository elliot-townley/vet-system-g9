import java.io.Serializable;
import java.time.LocalDate;

public class pet implements Serializable {
    private int petRefNum;
    private String surgeryName;
    private String name;
    private String species;
    private LocalDate regDate;



    public pet(int petRefNum, String surgeryName, String name, String species, LocalDate regDate) {
        this.setPetRefNum(petRefNum);
        this.setSurgeryName(surgeryName);
        this.setName(name);
        this.setSpecies(species);
        this.setRegDate(regDate);
    }

    private void setPetRefNum(int petRefNum) {
        this.petRefNum = petRefNum;
    }


    public int  getPetRefNum() {
        return petRefNum;
    }


    public int getPetRefNum(pet p) {
        return petRefNum;
    }



    public  int getPetRefNum(int i) {
        return petRefNum;
    }

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies(){
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public LocalDate getRegDate(){
        return regDate;
    }

    public void setRegDate(LocalDate regdate) {
        this.regDate = regdate;
    }
    public String getSurgeryName() {
        return surgeryName;
    }

    public void setSurgeryName(String surgeryName) {
        this.surgeryName = surgeryName;
    }
    public static void getPet( pet p){
        System.out.printf("Reference number: %d Name: %s Species: %s Registered Date: %s%n", p.getPetRefNum(p),p.getName(),p.getSpecies(),p.getRegDate());


    }


}



