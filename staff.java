import java.io.Serializable;

public class staff implements Serializable {


    private  int staffRefNum;
    private String surgeryName;
    private String type;

    private String staffName;

    public staff(int staffRefNum,String surgeryName, String type, String staffName) {
        this.staffRefNum = staffRefNum;
        this.surgeryName = surgeryName;
        this.type = type;
        this.staffName = staffName;
    }

    public int getStaffRefNum() {
        return staffRefNum;
    }



    public String getType() {
        return type;
    }

    public String getStaffName() {
        return staffName;
    }

    public String getSurgeryName() {
        return surgeryName;
    }

    public void setSurgeryName(String surgeryName) {
        this.surgeryName = surgeryName;
    }

    public static void getStaff(staff s){
        System.out.printf("Reference number: %d Surgery %s Type: %s Name: %s%n", s.getStaffRefNum(),s.getStaffName(),s.getType(),s.getStaffName());


    }

}