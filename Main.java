import java.io.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.chrono.ChronoLocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;

public class Main {

    private static String selectedSurgery;


     static  Scanner console =  new Scanner(System.in);
    private static final String PATHBooking = "M:\\vetDataStore\\";
    private static final String PATHPet = "M:\\vetDataStore\\";
    private static final String PATHStaff = "M:\\vetDataStore\\";
    private static final String PATHSurgery = "M:\\vetDataStore\\";

    private static ArrayList<booking> bookings = new ArrayList<booking>();
    private static ArrayList<pet> pets = new ArrayList<pet>();
    private static ArrayList<staff>staffs = new ArrayList<staff>();
    private static ArrayList<surgery>surgeries = new ArrayList<surgery>();



    private static void deSerialize(){
        ObjectInputStream oisBookings;
        ObjectInputStream oisPets;
        ObjectInputStream oisStaff;
        ObjectInputStream oisSurgery;

        try{
            oisBookings = new ObjectInputStream(new FileInputStream(PATHBooking + "booking.ser"));
            oisPets = new ObjectInputStream(new FileInputStream(PATHPet + "pet.ser"));
            oisStaff = new ObjectInputStream(new FileInputStream(PATHStaff + "staff.ser"));
            oisSurgery = new ObjectInputStream(new FileInputStream(PATHSurgery + "surgery.ser"));

            bookings = (ArrayList<booking>)oisBookings.readObject();
            pets = (ArrayList<pet>)oisPets.readObject();
            staffs = (ArrayList<staff>)oisStaff.readObject();
            surgeries =(ArrayList<surgery>)oisSurgery.readObject();

            oisBookings.close();
            oisPets.close();
            oisStaff.close();
            oisSurgery.close();

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    private static void serialize() {
        ObjectOutputStream oosBookings;
        ObjectOutputStream oosPets;
        ObjectOutputStream oosStaff;
        ObjectOutputStream oosSurgery;

        try {
            oosBookings = new ObjectOutputStream(new FileOutputStream(PATHBooking + "booking.ser"));
            oosPets = new ObjectOutputStream(new FileOutputStream(PATHPet + "pet.ser"));
            oosStaff = new ObjectOutputStream(new FileOutputStream(PATHStaff + "staff.ser"));
            oosSurgery = new ObjectOutputStream(new FileOutputStream(PATHSurgery + "surgery.ser"));

            oosBookings.writeObject(bookings);
            oosPets.writeObject(pets);
            oosStaff.writeObject(staffs);
            oosSurgery.writeObject(surgeries);


            oosBookings.close();
            oosPets.close();
            oosStaff.close();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
   }


    /**
     * This method is to select which surgery the user is from
     */
   // Note: surgery Select
    public static void surgerySelect(){
        boolean validSurgery = false;
      do {
          System.out.println("What surgery are you from?");
          selectedSurgery = console.nextLine();
          for (surgery s : surgeries) {
              if (selectedSurgery.equals(s.getName())) {
                  validSurgery = true;
                  break;
              }
          }
          if (!validSurgery) {
              System.out.println("The surgery name given is invalid. ");
          }
      }while (!validSurgery);
    }


    /**
     * This method is to search for a specific member of staff
     */
   // Note: Search for a member of staff
    public static void staffSearch(){
        System.out.println("What is the staff members name? ");
        String chosenStaffName = console.next();
        boolean found = false;

        for(staff s: staffs) {
            if( s.getStaffName().equals(chosenStaffName) && s.getSurgeryName().equals(selectedSurgery)){
                staff.getStaff(s);
                found = true;
            }

        }
        if(!found){
            System.out.println("Staff member not found!");
        }

    }

    /**
     * This method creates a new pet
     */
    //Note: add pet
    public static void addNewPet(){
        int newRefNum;
        String newPetName;
        String newPetSpecies;
        LocalDate newRegDate;
        int highestRefNum = 0;


        for(pet p: pets){
            if(p.getPetRefNum() >= highestRefNum){
                highestRefNum = p.getPetRefNum();
            }
        }

        System.out.println("What is the pets name? " );
        newPetName = console.nextLine();

        System.out.println("what is the species? ");
        newPetSpecies = console.nextLine();

        newRegDate = LocalDate.now();
        newRefNum = highestRefNum + 1;
        System.out.println(newRefNum);

        pets.add(new pet(newRefNum,selectedSurgery,newPetName,newPetSpecies, newRegDate));
    }

    /**
     * This method removes a pet
     */
    //Note: Remove a pet
    public static void removePet(){
        String chosenPet;

        System.out.println("What is the name of the pet?");
        chosenPet = console.nextLine();

        //ToDo:add a catch if not equal to
        pets.removeIf(p -> chosenPet.equals(p.getName()));

    }

    /**
     * This will remove a staff member
     */
    public static void removeStaff(){
        String chosenStaff;

        System.out.println("What is the staff name?");
        chosenStaff = console.nextLine();

        //ToDo:add a catch if not equal to
        staffs.removeIf(s -> chosenStaff.equals(s.getStaffName()));

    }

    /**
     * This will be used to create a booking
     */
    public static void createBooking(){
        String chosenPet;
        LocalDate bookedDate = LocalDate.now();
        String chosenStaffType;
        String chosenStaff;
        int newBookingNum;
        int staffNum = 0;
        int petNum = 0;
        String chosenTime;
        LocalDate fridayCheck;
        LocalDateTime timecheck;
        LocalTime time;
        int highestBookingNum = 0;
        boolean dateFound = true;
        boolean vaildType = false;
        boolean vaidPet = false;

        for(booking b: bookings){
            if(b.getBookingNumber() >= highestBookingNum){
                highestBookingNum = b.getBookingNumber();
            }
        }
        newBookingNum = highestBookingNum + 1;


        do {
            System.out.println("What pet do you want to make a booking for?");
            chosenPet = console.nextLine();

            for (pet p: pets){
                if(chosenPet.equals(p.getName()) && p.getSurgeryName().equals(selectedSurgery)){
                    petNum = p.getPetRefNum();
                    vaidPet = true;
                }
            }
        }while (!vaidPet);
        do {
            System.out.println("What staff type is needed?");
            chosenStaffType = console.nextLine();

            for(staff s: staffs){
                if(chosenStaffType.equals(s.getType())){
                    vaildType = true;
                    break;
                }
            }
        }while (!vaildType);


        //NOTE: find available date
        do  {
            System.out.println("Enter date and time (yyyy-MM-ddTHH:mm:ss): ");
            chosenTime = console.nextLine();
            LocalDateTime appointmentTime = LocalDateTime.parse(chosenTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            DayOfWeek dayOfWeek = appointmentTime.getDayOfWeek();
            fridayCheck = appointmentTime.toLocalDate();
            time = appointmentTime.toLocalTime();


            for( surgery s: surgeries) {
                timecheck = LocalDateTime.of(fridayCheck, s.getTrainingStart());
                if (dayOfWeek == DayOfWeek.FRIDAY && appointmentTime.isAfter(timecheck)) {
                    System.out.println("The time given is staff training");
                }
                if(time.isBefore(s.getOpeningTime())|| (time.isAfter(s.getClosingTime()))){
                    System.out.println("The time given is not within the opening times");

                }

            }

            for (booking b : bookings) {
                if (appointmentTime == b.getAppointmentDate()) {
                    System.out.println("This appointment Time is currently unavailable: ");

                    dateFound = false;

                }

            }



        } while (!dateFound);

        //NOTE: retrieve new booking number
        for (booking b: bookings){
            if(b.getBookingNumber() >= highestBookingNum ){
                highestBookingNum = b.getPetRefNum();
            }
        }
        highestBookingNum = highestBookingNum + 1;

        //NOTE: find staff num
        for(staff s: staffs){
            if(s.getType().equals(chosenStaffType)){
                System.out.println(s.getStaffName());

            }

        }

        System.out.println("Select the staff name needed?");
        chosenStaff = console.nextLine();
        for(staff s: staffs){
            if(s.getStaffName().equals(chosenStaff)){
                staffNum = s.getStaffRefNum();
            }
        }


        bookings.add(new booking(newBookingNum,selectedSurgery,bookedDate,LocalDateTime.parse(chosenTime),staffNum, petNum));
        System.out.println("You've successfully made a booking for " + chosenPet + " at " + chosenTime);

    }


    /**
     * This will search the pets
     */
    public static void petSearch(){
        System.out.println("What is the pet members name? ");
        String chosenPetName = console.next();

        for(pet p: pets) {
            if( p.getName().equals(chosenPetName) && p.getSurgeryName().equals(selectedSurgery)){
                pet.getPet(p);

            }
        }


    }


    /**
     * This is to search the bookings
     */
    public static void bookingSearch(){
        System.out.println("What is the pet members name? ");
        String chosenPetName = console.next();
        int petNum =  0;

        for(pet p: pets) {
            if( p.getName().equals(chosenPetName)){
                petNum = p.getPetRefNum();

            }
        }

        for(booking b: bookings) {
            if( b.getPetRefNum() == petNum){
                booking.getBooking(b);

            }
        }


    }


    /**
     * This method allows the user to create a new staff member
     *
     */
    public static void staffCreate() {

        int newStaffRefNum = 0;
        String newStaffType = "";
        String newStaffName = "";
        boolean option = false;
        int newRefNum;


        for(staff s: staffs){
            if(s.getStaffRefNum() >= newStaffRefNum){
                newStaffRefNum = s.getStaffRefNum();
            }
        }

        do {
            System.out.println("What is the type of the new Staff? (Nurse / Surgeon): ");
            newStaffType = console.next();

            //if, if else and else statements for exception handling
            if (newStaffType.equalsIgnoreCase("Nurse")) {
                System.out.println("You have selected: Nurse");
                newStaffType = "Nurse";
                option = true;

            } else if (newStaffType.equalsIgnoreCase("Surgeon")) {
                System.out.println("You have selected: Surgeon");
                newStaffType = "Surgeon";
                option = true;

                //Prompts the user to enter their value again if not matching a recognised input
            } else {
                System.out.println("You have entered an invalid input, please try again");
            }
        } while (option != true);


        System.out.println("What is the name of the new Staff?: ");
        newStaffName = console.next();
        System.out.printf("You have entered: %s\n", newStaffName);


        newRefNum = newStaffRefNum + 1;
        System.out.printf("The new staff members reference number is: %d\n", newRefNum);

        staffs.add(new staff(newRefNum,selectedSurgery,newStaffType,newStaffName));



    }


    /**
     * This is to remove a booking
     */
    public static void removeBooking(){
        int chosenRefNum = 0;
        String chosenName;
        int selectedBooking;

        System.out.println("What is the name of the pet?");
        chosenName = console.nextLine();
        for (pet p: pets){
            if(chosenName.equals(p.getName())){
                chosenRefNum = p.getPetRefNum();
            }
        }
        for(booking b: bookings){
            if(chosenRefNum == b.getPetRefNum()){
                booking.getBooking(b);
            }
        }

        System.out.println("Enter the booking number for the appointment you wish to cancel: ");
        selectedBooking = console.nextInt();

        //Note: Maybe add to other loop to be more efficient?
        for(booking b: bookings){
            if(b.getPetRefNum() == selectedBooking){
                bookings.remove(b);
            }
        }

        System.out.println("You have deleted the booking for: " + chosenName);

    }

    public static void main (String[]args) throws IOException {



        deSerialize();

        // Note: debugging
       /* bookings.clear();
        pets.clear();
        staffs.clear();



        // Note: booking data
        bookings.add(new booking(1001,"Welsh Vets", LocalDate.parse("2024-02-05"), LocalDateTime.parse("2024-02-18T11:30:00"), 101, 10002));
        bookings.add(new booking(1002,"Welsh Vets", LocalDate.parse("2024-02-05"), LocalDateTime.parse("2024-02-18T14:45:00"), 104, 10009));
        bookings.add(new booking(1003,"Welsh Vets", LocalDate.parse("2024-02-05"), LocalDateTime.parse("2024-02-18T16:00:00"), 102, 10004));
        bookings.add(new booking(1004,"Safe Haven", LocalDate.parse("2024-02-06"), LocalDateTime.parse("2024-02-18T10:15:00"), 103, 10001));
        bookings.add(new booking(1005,"Safe Haven", LocalDate.parse("2024-02-06"), LocalDateTime.parse("2024-02-18T12:30:00"), 103, 10010));
        bookings.add(new booking(1006,"Safe Haven", LocalDate.parse("2024-02-07"), LocalDateTime.parse("2024-02-18T13:15:00"), 101, 10007));

        //Note: pet data
        pets.add(new pet(10001,"Safe Haven", "Jerry", "Cat", LocalDate.parse("2018-12-14")));
        pets.add(new pet(10002, "Safe Haven","Tom", "Dog", LocalDate.parse("2020-02-20")));
        pets.add(new pet(10003, "Safe Haven","Jake", "Lizard", LocalDate.parse("2017-09-20")));
        pets.add(new pet(10004, "Safe Haven","Jess", "Parrots", LocalDate.parse("2021-05-21")));
        pets.add(new pet(10005, "Safe Haven","Cake", "Hamster", LocalDate.parse("2023-08-21")));
        pets.add(new pet(10006, "Welsh Vets","Reginald", "Rabbit", LocalDate.parse("2022-02-23")));
        pets.add(new pet(10007,"Welsh Vets" ,"Buddy", "Fish", LocalDate.parse("2024-01-07")));
        pets.add(new pet(10008,"Welsh Vets" ,"Bear", "Donkey", LocalDate.parse("2004-10-24")));
        pets.add(new pet(10009, "Welsh Vets","Cooper", "Ferret", LocalDate.parse("2018-06-25")));
        pets.add(new pet(10010, "Welsh Vets","Lily", "Chicken", LocalDate.parse("2020-02-25")));

        //Note: staff data
        staffs.add(new staff(101,"Welsh Vets", "Nurse", "Anna"));
        staffs.add(new staff(102,"Welsh Vets" ,"Surgeon", "Dave"));
        staffs.add(new staff(103, "Safe Haven","Surgeon", "Christoff"));
        staffs.add(new staff(104, "Safe Haven","Nurse", "Diana"));

        //Note: surgery data
        surgeries.add(new surgery("Safe Haven", "LL34 6NU", LocalTime.parse("09:00:00"), LocalTime.parse("17:00:00"), LocalTime.parse("12:00:00"),LocalTime.parse("17:00:00")));
        surgeries.add(new surgery("Welsh Vets", "LL21 0PE", LocalTime.parse("09:00:00"), LocalTime.parse("17:00:00"), LocalTime.parse("12:00:00"),LocalTime.parse("17:00:00")));
*/

        //Note: for debugging
        for( booking b: bookings){
            System.out.println(b.toString());
        }




        String choice = null;




        surgerySelect();



        do{

            System.out.println("Select An Aspect of the system: ");
            System.out.println("1: Make a Booking");
            System.out.println("2: Cancel a Booking");
            System.out.println("3: Search a Booking");
            System.out.println("4: Register a Staff");
            System.out.println("5: Delete a Staff");
            System.out.println("6: Search a Staff");
            System.out.println("7: Register a Pet");
            System.out.println("8: Delete a Pet");
            System.out.println("9: Search a Pet");
            System.out.println("?: Repeat System Aspects");
            System.out.println("q: Exit System");
            choice = console.nextLine();

            switch (choice) {
                case "1":
                    System.out.println("This will make a booking");
                    createBooking();
                    break;
                case "2":
                    System.out.println("This will cancel a booking");
                    removeBooking();

                    break;
                case "3":
                    System.out.println("This will search a booking");
                    bookingSearch();

                    break;
                case "4":
                    System.out.println("This will register a staff");
                    staffCreate();

                    break;
                case "5":
                    System.out.println("This will delete a staff");
                    removeStaff();
                    break;
                case "6":
                    System.out.println("This will search a staff");
                    staffSearch();
                    break;
                case "7":
                    System.out.println("This would register a pet");
                    addNewPet();
                    System.out.println("Enter '?' for options");
                    break;
                case "8":
                    System.out.println("This will delete a pet");
                    removePet();
                    System.out.println("Enter '?' for options");
                    break;
                case "9":
                    System.out.println("This will search a pet");
                    petSearch();

                    break;
                case "?":
                    System.out.println("Select An Aspect of the system: ");
                    System.out.println("1: Make a Booking");
                    System.out.println("2: Cancel a Booking");
                    System.out.println("3: Search a Booking");
                    System.out.println("4: Register a Staff");
                    System.out.println("5: Delete a Staff");
                    System.out.println("6: Search a Staff");
                    System.out.println("7: Register a Pet");
                    System.out.println("8: Delete a Pet");
                    System.out.println("9: Search a Pet");
                    System.out.println("?: Repeat System Aspects");
                    System.out.println("q: Exit System");
                    break;
                case "q":
                    System.out.println("Goodbye");
                    break;
            }

        }while (!choice.equals("q"));

        serialize();


    }
}
