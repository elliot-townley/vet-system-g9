import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;


public class booking implements Serializable {

    private int bookingNumber;
    private String surgeryName;
    private LocalDate dateBooked;
    private LocalDateTime appointmentDate;
    private int staffRefNum;
    private int petRefNum;

    public booking(int bookingNumber,String surgeryName, LocalDate dateBooked, LocalDateTime appointmentDate, int staffRefNum, int petRefNum) {
        this.bookingNumber = bookingNumber;
        this.surgeryName = surgeryName;
        this.dateBooked = dateBooked;
        this.appointmentDate = appointmentDate;
        this.staffRefNum = staffRefNum;
        this.petRefNum = petRefNum;
    }







    @Override
    public String toString() {
        return String.format("booking Number: %04d  surgery: %s booked on: %s  date of appointment: %s  staff: %03d  pet: %05d",
                bookingNumber,surgeryName, dateBooked, appointmentDate, staffRefNum, petRefNum);
    }




    public int getBookingNumber() {
        return bookingNumber;
    }

    public void setBookingNumber(int bookingNumber) {
        this.bookingNumber = bookingNumber;
    }

    public LocalDate getDateBooked() {
        return dateBooked;
    }

    public void setDateBooked(LocalDate dateBooked) {
        this.dateBooked = dateBooked;
    }

    public LocalDateTime getAppointmentDate() {
        return appointmentDate;
    }



    public void setAppointmentDate(LocalDateTime appointmentDate) {
        this.appointmentDate = appointmentDate;
    }


    public int getStaffRefNum() {
        return staffRefNum;
    }

    public void setStaffRefNum(int staffRefNum) {
        this.staffRefNum = staffRefNum;
    }

    public int getPetRefNum() {
        return petRefNum;
    }

    public void setPetRefNum(int petRefNum) {
        this.petRefNum = petRefNum;
    }

    public String getSurgeryName() {
        return surgeryName;
    }

    public void setSurgeryName(String surgery) {
        this.surgeryName = surgeryName;
    }

    public static void getBooking(booking b){
        System.out.printf("Booking number: %d Surgery: %s Pet reference number: %d Staff reference number: %s%n", b.getBookingNumber(), b.getSurgeryName(),b.getPetRefNum(),b.getStaffRefNum());


    }
}
